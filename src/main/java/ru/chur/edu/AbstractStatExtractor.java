package ru.chur.edu;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractStatExtractor implements StatExtractor {

    private final String contentType;

    protected AbstractStatExtractor() {
        this(null);
    }

    protected AbstractStatExtractor(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public DirectionStat extract(Direction direction) {
        DirectionStat directionStat = new DirectionStat();

        try {
            Document document = getDocument(direction.getUrl());
            Elements tdList = document.getElementsByTag("td");
            List<Applicant> applicantList = new ArrayList<>();
            Iterator<Element> tds = tdList.iterator();
            Applicant applicant;
            int totalScore = 0;
            int totalCommon = 0;
            while (tds.hasNext()) {
                applicant = extractApplicant(tds);
                if (applicant != null) {
                    if (applicant.getCondition() == ConditionEnum.COMMON) {
                        totalScore += applicant.getScore();
                        totalCommon++;
                    }
                    applicantList.add(applicant);
                }
            }
            directionStat.setApplicantList(applicantList);
            directionStat.setTotalApplicationNum(totalCommon);
            directionStat.setTotalPlaces(direction.getTotalPlaces() - (applicantList.size() - totalCommon));
            directionStat.setAvgScore(totalScore/totalCommon);
        } catch (IOException e) {
            LoggerFactory.getLogger(this.getClass()).error(e.getMessage(), e);
            throw new IllegalStateException(e);
        }

        return directionStat;
    }

    protected abstract Applicant extractApplicant(Iterator<Element> elementList);

    protected Document getDocument(String url) throws IOException {
        Connection conn = Jsoup.connect(url).userAgent("Mozilla");
        if (this.contentType != null) {
            conn = conn.header("Content-Type", contentType);
        }
        return conn.get();
    }

    protected static void parseName(Applicant applicant, String name) {
        String[] fio = name.trim().split(" ");
        applicant.setLastName(fio[0]);
        if (fio.length > 3) {
            applicant.setMiddleName(fio[fio.length-1]);
            StringBuilder out = new StringBuilder(fio[1]);
            for (int i=2; i<fio.length-2; i++) {
                out.append(' ');
                out.append(fio[i]);
            }
            applicant.setFirstName(out.toString());
        } else if (fio.length == 3) {
            applicant.setFirstName(fio[1]);
            applicant.setMiddleName(fio[2]);
        } else if (fio.length == 2) {
            applicant.setFirstName(fio[1]);
        }
    }

    public String getContentType() {
        return contentType;
    }
}
