package ru.chur.edu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import ru.chur.edu.amech.ArmyMechStatExtractor;
import ru.chur.edu.eltech.EltechStatExtractor;
import ru.chur.edu.ifmo.IfmoExtractor;
import ru.chur.edu.spbu.SpbuExtractor;

@Configuration
public class AppConfig {

    @Bean
    public List<University> universityList() {
        List<University> list = new ArrayList<University>();

        list.add(createAmech());
        list.add(createSpbu());
        list.add(createLtech());
        list.add(createIfmo());

        return Collections.unmodifiableList(list);
    }

    @Bean
    public University createIfmo() {
        List<Direction> directionList = new ArrayList<>();

        directionList.add(new Direction("010400.1", "ЕНФ - Прикладная математика и информатика", 20,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=2&spid=1", EntranceSetEnum.MRI));

        directionList.add(new Direction("010400.2", "ФИТиП - Прикладная математика и информатика", 95,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=1&spid=1", EntranceSetEnum.MRI));

        directionList.add(new Direction("200400.1", "ФОИСТ - Оптотехника", 55,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=6&spid=15", EntranceSetEnum.MRI));

        directionList.add(new Direction("221000.1", "ФКТиУ - Мехатроника и робототехника", 20,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=3&spid=20", EntranceSetEnum.MRI));

        directionList.add(new Direction("221000.2", "ФТМиТ - Мехатроника и робототехника", 18,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=4&spid=20", EntranceSetEnum.MRI));

        directionList.add(new Direction("221000.2", "ФТМиТ - Мехатроника и робототехника", 18,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=3&spid=20", EntranceSetEnum.MRI));

        directionList.add(new Direction("230100.1", "ФКТиУ - Информатика и вычислительная техника", 60,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=4&spid=13", EntranceSetEnum.MRF));

        directionList.add(new Direction("230100.2", "ФТМиТ - Информатика и вычислительная техника", 22,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=3&spid=13", EntranceSetEnum.MRI));

        directionList.add(new Direction("230100.3", "ЕНФ - Информатика и вычислительная техника", 20,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=2&spid=2", EntranceSetEnum.MRI));

        directionList.add(new Direction("230100.4", "ФИТиП - Информатика и вычислительная техника", 35,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=1&spid=2", EntranceSetEnum.MRI));

        directionList.add(new Direction("230100.5", "ФКТиУ - Информатика и вычислительная техника", 20,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=4&spid=2", EntranceSetEnum.MRI));

        directionList.add(new Direction("230100.6", "ФТМиТ - Информатика и вычислительная техника", 20,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=3&spid=2", EntranceSetEnum.MRI));

        directionList.add(new Direction("230700.1", "ФИТиП - Прикладная информатика", 15,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=1&spid=6", EntranceSetEnum.MRI));

        directionList.add(new Direction("230700.2", "ФФиОИ - Прикладная информатика", 20,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=13&spid=6", EntranceSetEnum.MRI));

        directionList.add(new Direction("200700", "ФФиОИ - Фотоника и оптоинформатика (Информатика)", 22,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=13&spid=57", EntranceSetEnum.MRI));

        directionList.add(new Direction("090900.1", "ИКВО - Информационная безопасность", 22,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=8&spid=8", EntranceSetEnum.MRI));

        directionList.add(new Direction("090900.2", "ФКТиУ - Информационная безопасность (БИТ)", 43,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=4&spid=58", EntranceSetEnum.MRI));

        directionList.add(new Direction("090900.3", "ФКТиУ - Информационная безопасность (ПБКС)", 18,
                "http://abit.ifmo.ru/abitUMS/statByUsers.htm?fid=1&edform=1&facid=4&spid=59", EntranceSetEnum.MRI));

        University university = new University("ИТМО", "ИТМО");
        university.setDirectionList(directionList);

        return university;
    }

    @Bean
    public University createSpbu() {
        List<Direction> directionList = new ArrayList<Direction>();
        
        directionList.add(new Direction("010400.2", "ПМПУ - Прикладная математика и информатика", 180,
                "http://cabinet.spbu.ru/Lists/1k_EntryLists/list1_1_1_12_434_.html", EntranceSetEnum.MRI));

        directionList.add(new Direction("010800.0", "МатМех - Механика и математическое моделирование, МРИ", 22,
                "http://cabinet.spbu.ru/Lists/1k_EntryLists/list1_1_1_15_437_.html", EntranceSetEnum.MRI));

        directionList.add(new Direction("010900.2", "ПМПУ - Прикладные математика, физика и процессы управления", 30,
                "http://cabinet.spbu.ru/Lists/1k_EntryLists/list1_1_1_1806_439_.html", EntranceSetEnum.MRI));

        directionList.add(new Direction("010400.1", "МатМех - Прикладная математика и информатика", 70,
                "http://cabinet.spbu.ru/Lists/1k_EntryLists/list1_1_1_11_434_.html", EntranceSetEnum.MRI));

        directionList.add(new Direction("010701.2", "МатМех - Фундаментальная математика и механика. Механика", 10,
                "http://cabinet.spbu.ru/Lists/1k_EntryLists/list1_1_1_14_405_bae1d41c-5ec2-49f4-9590-7514cfdc55b0.html", EntranceSetEnum.MRI));

        University university = new University("СПбГУ", "Университет");
        university.setDirectionList(directionList);

        return university;
    }

    @Bean
    public University createLtech() {
        List<Direction> directionList = new ArrayList<>();
        University university = new University("ЛЭТИ", "Электротехнический университет «ЛЭТИ»");
        directionList.add(new Direction("010400.62", "Прикладная математика и информатика", 50,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/01040062-prikladnaya-matematika-i-informatika",
                EntranceSetEnum.MRF));
        directionList.add(new Direction("230100.62", "Информатика и вычислительная техника", 105,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/23010062-informatika-i-vychislitelnaya-tehnika",
                EntranceSetEnum.MRI));
        directionList.add(new Direction("230400.62", "Информационные системы и технологии", 60,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/23040062-informacionnye-sistemy-i-tehnologii",
                EntranceSetEnum.MRI));
        directionList.add(new Direction("090301.65", "Компьютерная безопасность", 50,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/09030165-kompyuternaya-bezopasnost",
                EntranceSetEnum.MRF));
        directionList.add(new Direction("210700.62", "Инфокоммуникационные технологии и системы связи", 50,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/21070062-infokommunikacionnye-tehnologii-i-sistemy-svyazi",
                EntranceSetEnum.MRF));
        directionList.add(new Direction("221400.62", "Управление качеством", 21,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/21070062-infokommunikacionnye-tehnologii-i-sistemy-svyazi",
                EntranceSetEnum.MRI));
        directionList.add(new Direction("222000.62", "Инноватика", 20,
                "http://www.eltech.ru/ru/abiturientam/priyom-na-1-y-kurs/prinyatye-zayavleniya/21070062-infokommunikacionnye-tehnologii-i-sistemy-svyazi",
                EntranceSetEnum.MRI));

        university.setDirectionList(directionList);

        return university;
    }


    @Bean
    public University createAmech() {
        List<Direction> directionList = new ArrayList<>();
        University university = new University("ВоенМех", "Военно-Механический Университет им.Устинова");
        directionList.add(new Direction("230100Б", "Информатика и вычислительная техника", 23,
                "http://voenmeh.ru/files/0/230100B.htm",
                EntranceSetEnum.MRI));
        directionList.add(new Direction("230400Б", "Информационные системы и технологии", 19,
                "http://voenmeh.ru/files/0/230400B.htm",
                EntranceSetEnum.MRI));
        university.setDirectionList(directionList);
        return university;
    }

    @Bean(name = "СПбГУ")
    public StatExtractor createSpbuExtractor() {
        return new SpbuExtractor();
    }

    @Bean(name = "ЛЭТИ")
    public StatExtractor createLtechExtractor() {
        return new EltechStatExtractor();
    }

    @Bean(name = "ИТМО")
    public StatExtractor createIfmoExtractor() {
        return new IfmoExtractor();
    }

    @Bean(name = "ВоенМех")
    public StatExtractor createArmyMechStatExtractor() {
        return new ArmyMechStatExtractor();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
