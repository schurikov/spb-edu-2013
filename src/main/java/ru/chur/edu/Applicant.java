package ru.chur.edu;

import java.util.Date;

import org.springframework.util.StringUtils;

public class Applicant {
    private String lastName;
    private String firstName;
    private String middleName;
    private Date applicationDate;
    private ConditionEnum condition;
    private int score;
    private Boolean originalDocs;
    private Integer priority;

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getOriginalDocs() {
        return originalDocs;
    }

    public void setOriginalDocs(Boolean originalDocs) {
        this.originalDocs = originalDocs;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public Date getApplicationDate() {
        return applicationDate;
    }
    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }
    public ConditionEnum getCondition() {
        return condition;
    }
    public void setCondition(ConditionEnum condition) {
        this.condition = condition;
    }

    public String getName() {
        StringBuilder out = new StringBuilder(lastName);
        if (StringUtils.hasText(firstName)) {
            out.append(' ');
            out.append(firstName);
        }
        if (StringUtils.hasText(middleName)) {
            out.append(' ');
            out.append(middleName);
        }
        return out.toString();
    }

    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Applicant [lastName=");
        builder.append(lastName);
        builder.append(", firstName=");
        builder.append(firstName);
        builder.append(", middleName=");
        builder.append(middleName);
        builder.append(", applicationDate=");
        builder.append(applicationDate);
        builder.append(", condition=");
        builder.append(condition);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Applicant other = (Applicant) obj;
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (middleName == null) {
            if (other.middleName != null) {
                return false;
            }
        } else if (!middleName.equals(other.middleName)) {
            return false;
        }
        return true;
    }

}
