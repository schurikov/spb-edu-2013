package ru.chur.edu;

public class Direction {
    private final String id;
    private final String name;
    private final String url;
    private final int totalPlaces;
    private final EntranceSetEnum entranceSetEnum;

    private DirectionStat directionStat;

    public Direction(String id, String name, int totalPlaces, String url, EntranceSetEnum entranceSetEnum) {
        this.id = id;
        this.name = name;
        this.totalPlaces = totalPlaces;
        this.url = url;
        this.entranceSetEnum = entranceSetEnum;
    }

    public EntranceSetEnum getEntranceSetEnum() {
        return entranceSetEnum;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getTotalPlaces() {
        return totalPlaces;
    }

    public String getUrl() {
        return url;
    }

    public DirectionStat getDirectionStat() {
        return directionStat;
    }

    public void setDirectionStat(DirectionStat directionStat) {
        this.directionStat = directionStat;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Direction [id=");
        builder.append(id);
        builder.append(", name=");
        builder.append(name);
        builder.append(", totalPlaces=");
        builder.append(totalPlaces);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Direction other = (Direction) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}
