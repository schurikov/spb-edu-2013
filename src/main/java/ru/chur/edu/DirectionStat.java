package ru.chur.edu;

import java.util.Collections;
import java.util.List;

public class DirectionStat {
    private int totalPlaces;
    private int totalApplicationNum = 0;
    private int avgScore = 0;
    private List<Applicant> applicantList;

    public int getTotalPlaces() {
        return totalPlaces;
    }

    public void setTotalPlaces(int totalPlaces) {
        this.totalPlaces = totalPlaces;
    }

    public int getTotalApplicationNum() {
        return totalApplicationNum;
    }

    public void setTotalApplicationNum(int totalApplicationNum) {
        this.totalApplicationNum = totalApplicationNum;
    }

    public List<Applicant> getApplicantList() {
        return Collections.unmodifiableList(applicantList);
    }

    public void setApplicantList(List<Applicant> applicantList) {
        this.applicantList = applicantList;
    }

    public int getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(int avgScore) {
        this.avgScore = avgScore;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DirectionStat [totalPlaces=");
        builder.append(totalPlaces);
        builder.append(", totalApplicationNum=");
        builder.append(totalApplicationNum);
        builder.append(", avgApplOnPlace=");
        builder.append(totalApplicationNum/totalPlaces);
        builder.append(", avgScore=");
        builder.append(avgScore);
        builder.append("]");
        return builder.toString();
    }

}
