package ru.chur.edu;

public enum EntranceSetEnum {
    MRI("МРИ"),
    MRF("МРФ");
    
    private final String code;
    
    private EntranceSetEnum(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
