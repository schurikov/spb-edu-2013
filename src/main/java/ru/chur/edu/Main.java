package ru.chur.edu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.StringUtils;

public final class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    private static final Logger STAT_LOGGER1 = LoggerFactory.getLogger("stat.log1");
    private static final Logger STAT_LOGGER2 = LoggerFactory.getLogger("stat.log2");

    private Main() {
    }

    @SuppressWarnings({ "resource", "unchecked" })
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        List<University> universityList = context.getBean("universityList", List.class);
        for(University university : universityList) {
            LOG.info("{}", university);
            StatExtractor extractor = context.getBean(university.getKey(), StatExtractor.class);
            for(Direction direction : university.getDirectionList()) {
                LOG.info("{}", direction);
                DirectionStat stat = extractor.extract(direction);
                LOG.info("{}", stat);
                direction.setDirectionStat(stat);
            }
        }
        LOG.info("Looking for dubs");
        Map<Applicant, List<Dups>> apMap = new HashMap<>();
        for(University university : universityList) {
            for(Direction direction : university.getDirectionList()) {
                for (Applicant applicant : direction.getDirectionStat().getApplicantList()) {
                    if (apMap.containsKey(applicant)) {
                        apMap.get(applicant).add(new Dups(applicant, university, direction));
                    } else {
                        List<Dups> dups = new ArrayList<>();
                        Applicant key = new Applicant();
                        key.setFirstName(applicant.getFirstName());
                        key.setLastName(applicant.getLastName());
                        key.setMiddleName(applicant.getMiddleName());
                        dups.add(new Dups(applicant, university, direction));
                        apMap.put(key, dups);
                    }
                }
            }
        }
        for (Map.Entry<Applicant, List<Dups>> entry : apMap.entrySet()) {
            if (entry.getValue().size() > 1) {
                Map<EntranceSetEnum, Integer> scoreMap = new HashMap<EntranceSetEnum, Integer>();
                for (Dups dups : entry.getValue()) {
                    if (dups.getApplicant().getScore() > 0) {
                        scoreMap.put(dups.getDirection().getEntranceSetEnum(), dups.getApplicant().getScore());
                    }
//                    LOG.warn("condition={}: {}, score={}:{}:{}", new Object[] {
//                            dups.getApplicant().getCondition(),
//                            dups.getApplicant().getName(), dups.getApplicant().getScore(),
//                            dups.getUniversity().getKey(), 
//                            dups.getDirection().getName()});
                }
                if (!scoreMap.isEmpty()) {
                    for (Dups dups : entry.getValue()) {
                        if (scoreMap.containsKey(dups.getDirection().getEntranceSetEnum())) {
                            dups.getApplicant().setScore(scoreMap.get(dups.getDirection().getEntranceSetEnum()));
                        }
                    }
                }
            }
        }
        for(University university : universityList) {
            LOG.info("{}", university);
            for(Direction direction : university.getDirectionList()) {
                LOG.info("{}", direction);
                int withoutScore = 0;
                int withScore = 0;
                for (Applicant applicant : direction.getDirectionStat().getApplicantList()) {
                    if (applicant.getCondition() == ConditionEnum.COMMON) {
                        if (applicant.getScore() == 0) {
                            withoutScore++;
                        } else {
                            withScore++;
                        }
                    }
                }
                LOG.info("withoutScore={}, withScore={}, total={}/{}",
                        withoutScore, withScore, withoutScore + withScore,
                        direction.getDirectionStat().getTotalApplicationNum());
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        for(University university : universityList) {
            for(Direction direction : university.getDirectionList()) {
                StringBuilder out = new StringBuilder();
                out.append("event_time=").append(sdf.format(new Date())).append(",");
                out.append("university_id=").append(university.getKey()).append(',');
                out.append("university_name='").append(university.getName()).append("',");
                out.append("direction_id='").append(direction.getId()).append("',");
                out.append("direction='").append(direction.getName()).append("',");
                out.append("total_places=").append(direction.getTotalPlaces()).append(",");
                out.append("entrance_set=").append(direction.getEntranceSetEnum());
                STAT_LOGGER1.info("{}", out);
            }
        }

        for(University university : universityList) {
            for(Direction direction : university.getDirectionList()) {
                for (Applicant applicant : direction.getDirectionStat().getApplicantList()) {
                    StringBuilder out = new StringBuilder();
                    if (applicant.getApplicationDate() != null) {
                        out.append("event_time=").append(sdf.format(applicant.getApplicationDate())).append(",");
                    } else {
                        out.append("event_time=").append(sdf.format(new Date())).append(",");
                    }
                    out.append("university_id=").append(university.getKey()).append(',');
                    out.append("direction_id='").append(direction.getId()).append("',");
                    out.append("direction='").append(direction.getName()).append("',");
                    out.append("last_name='").append(applicant.getLastName()).append("',");
                    if (StringUtils.hasText(applicant.getFirstName())) {
                        out.append("first_name='").append(applicant.getFirstName()).append("',");
                    }
                    if (StringUtils.hasText(applicant.getMiddleName())) {
                        out.append("middle_name='").append(applicant.getMiddleName()).append("',");
                    }
                    out.append("score=").append(applicant.getScore()).append(",");
                    if (applicant.getOriginalDocs() != null) {
                        out.append("original=").append(applicant.getOriginalDocs()).append(",");
                    }
                    if (applicant.getPriority() != null) {
                        out.append("priority=").append(applicant.getPriority()).append(",");
                    }
                    out.append("condition=").append(applicant.getCondition());
                    STAT_LOGGER2.info("{}", out);
                }
            }
        }
    }


    private static class Dups {
        private Applicant applicant;
        private University university;
        private Direction direction;
        public Dups(Applicant applicant, University university, Direction direction) {
            super();
            this.applicant = applicant;
            this.university = university;
            this.direction = direction;
        }
        public Applicant getApplicant() {
            return applicant;
        }
        public University getUniversity() {
            return university;
        }
        public Direction getDirection() {
            return direction;
        }
        
    }
}
