package ru.chur.edu;

public interface StatExtractor {
    DirectionStat extract(Direction direction);
}
