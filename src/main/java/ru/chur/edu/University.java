package ru.chur.edu;

import java.util.Collections;
import java.util.List;

public class University {
    private String key;
    private String name;
    private List<Direction> directionList;

    public University() {
    }

    public University(String key, String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Direction> getDirectionList() {
        return Collections.unmodifiableList(directionList);
    }

    public void setDirectionList(List<Direction> directionList) {
        this.directionList = directionList;
    }

    @Override
    public String toString() {
        return "University [key=" + key + ", name=" + name + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        University other = (University) obj;
        if (key == null) {
            if (other.key != null)
                return false;
        } else if (!key.equals(other.key))
            return false;
        return true;
    }


}
