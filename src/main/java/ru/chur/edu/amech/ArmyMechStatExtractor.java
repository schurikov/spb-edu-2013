package ru.chur.edu.amech;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.chur.edu.AbstractStatExtractor;
import ru.chur.edu.Applicant;
import ru.chur.edu.ConditionEnum;

import java.io.IOException;
import java.util.Iterator;

public class ArmyMechStatExtractor extends AbstractStatExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(ArmyMechStatExtractor.class);

    @Override
    protected Document getDocument(String url) throws IOException {
        Connection conn = Jsoup.connect(url).userAgent("Mozilla/5.0").
                header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        String body = new String(conn.execute().body().getBytes("windows-1251"));
        return Parser.parse(body, url);
    }

    @Override
    protected Applicant extractApplicant(Iterator<Element> elementList) {
        String lineNum = elementList.next().text().trim();
        if ("№".equals(lineNum)) {
            elementList.next(); //Name
            elementList.next(); //Score
            elementList.next(); //Original
            elementList.next(); //Condition
            return null;
        }
        Applicant applicant = new Applicant();
        parseName(applicant, elementList.next().text().trim());
        String score = elementList.next().text().trim();
        applicant.setScore(Integer.parseInt(score));
        String docCode = elementList.next().text();
        if ("подлинник".equals(docCode)) {
            applicant.setOriginalDocs(Boolean.TRUE);
        } else if ("копия".equals(docCode)) {
            applicant.setOriginalDocs(Boolean.FALSE);
        } else {
            throw new IllegalArgumentException(docCode);
        }
        String condition = elementList.next().text();
        if ("Гособоронзаказ".equals(condition) || "Целевой набор".equals(condition)) {
            applicant.setCondition(ConditionEnum.TARGET);
        } else if ("На общих основаниях".equals(condition)) {
            applicant.setCondition(ConditionEnum.COMMON);
        } else {
            throw new IllegalArgumentException(condition);
        }
        return applicant;
    }
}
