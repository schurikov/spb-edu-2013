package ru.chur.edu.eltech;

import java.util.Iterator;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.chur.edu.AbstractStatExtractor;
import ru.chur.edu.Applicant;
import ru.chur.edu.ConditionEnum;

public class EltechStatExtractor extends AbstractStatExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(EltechStatExtractor.class);

    @Override
    protected Applicant extractApplicant(Iterator<Element> elementList) {
        Applicant applicant = new Applicant();
        int numberLine = Integer.parseInt(elementList.next().text().trim());
        parseName(applicant, elementList.next().text().trim());
        String condition = elementList.next().text().trim();
        if ("БВИ".equals(condition) || "ВК".equals(condition)) {
            applicant.setCondition(ConditionEnum.PREFENTIAL);
        } else if ("ОК".equals(condition)) {
            applicant.setCondition(ConditionEnum.COMMON);
        } else if ("Ц".equals(condition)) {
            applicant.setCondition(ConditionEnum.TARGET);
        } else {
            throw new IllegalArgumentException(condition);
        }
        applicant.setOriginalDocs("да".equals(elementList.next().text().trim()));
        applicant.setPriority(Integer.parseInt(elementList.next().text().trim()));
        try {
            applicant.setScore(Integer.parseInt(elementList.next().text().trim()));
        } catch (NumberFormatException ignore) {}
        LOG.debug("{}.{}", numberLine, applicant);
        return applicant;
    }

}
