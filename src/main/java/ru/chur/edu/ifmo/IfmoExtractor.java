package ru.chur.edu.ifmo;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.chur.edu.AbstractStatExtractor;
import ru.chur.edu.Applicant;
import ru.chur.edu.ConditionEnum;

import java.util.Iterator;

public class IfmoExtractor extends AbstractStatExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(IfmoExtractor.class);

    @Override
    protected Applicant extractApplicant(Iterator<Element> elementList) {
        Applicant applicant = new Applicant();
        int numberLine = Integer.parseInt(elementList.next().text().trim());
        parseName(applicant, elementList.next().text().trim());
        applicant.setPriority(Integer.parseInt(elementList.next().text().trim()));
        elementList.next(); // Department

        String condition = elementList.next().text().trim();
        if ("Конкурс".equals(condition)) {
            applicant.setCondition(ConditionEnum.COMMON);
        } else if ("Без испытаний".equals(condition) || "Вне конкурса".equals(condition)) {
            applicant.setCondition(ConditionEnum.PREFENTIAL);
        } else if ("Целевое".equals(condition)) {
            applicant.setCondition(ConditionEnum.TARGET);
        } else if ("Не преодолён минимум по предметам".equals(condition)) {
            elementList.next();
            elementList.next();
            return null;
        } else {
            throw new IllegalArgumentException(condition);
        }
        try {
            applicant.setScore(Integer.parseInt(elementList.next().text().trim()));
        } catch (NumberFormatException ignore) {}
        applicant.setOriginalDocs("да".equals(elementList.next().text().trim()));
        LOG.debug("{}.{}", numberLine, applicant);
        return applicant;
    }

}
