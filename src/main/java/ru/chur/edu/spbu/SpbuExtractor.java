package ru.chur.edu.spbu;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.chur.edu.AbstractStatExtractor;
import ru.chur.edu.Applicant;
import ru.chur.edu.ConditionEnum;
import ru.chur.edu.Direction;
import ru.chur.edu.DirectionStat;
import ru.chur.edu.StatExtractor;

public class SpbuExtractor extends AbstractStatExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(SpbuExtractor.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    public SpbuExtractor() {
        super("text/html; charset=Cp1251");
    }

    @Override
    protected Applicant extractApplicant(Iterator<Element> elementList) {
        Applicant applicant = new Applicant();
        int numberLine = Integer.parseInt(elementList.next().text().trim());
        applicant.setLastName(elementList.next().text().trim());
        applicant.setFirstName(elementList.next().text().trim());
        applicant.setMiddleName(elementList.next().text().trim());
        String condition = elementList.next().text().trim();
        if ("б/э".equals(condition) || "в/к".equals(condition)) {
            applicant.setCondition(ConditionEnum.PREFENTIAL);
        } else if ("общ.".equals(condition)) {
            applicant.setCondition(ConditionEnum.COMMON);
        } else {
            throw new IllegalArgumentException(condition);
        }
        try {
            applicant.setApplicationDate(sdf.parse(elementList.next().text().trim()));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
        LOG.debug("{}.{}", numberLine, applicant);
        return applicant;
    }
}
