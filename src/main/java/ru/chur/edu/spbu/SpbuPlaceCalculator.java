package ru.chur.edu.spbu;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import ru.chur.edu.Applicant;
import ru.chur.edu.ConditionEnum;
import ru.chur.edu.Direction;
import ru.chur.edu.EntranceSetEnum;
import ru.chur.edu.University;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SpbuPlaceCalculator {
    private static final Logger LOG = LoggerFactory.getLogger(SpbuPlaceCalculator.class);
    private int totalApplicantCount;
    private int totalTakeoutDocCount;
    private int totalOtherRaceUsageCount;
    private int originalDocsCount;
    private int cheaterNumCount;

    public int calcPlace(String url, String lastName) {
        List<Applicant> applicantList = new ArrayList<>();
        Connection conn = Jsoup.connect(url).userAgent("Mozilla");
        conn = conn.header("Content-Type", "text/html; charset=Cp1251");
        try {
            Document document = conn.get();
            Elements tdList = document.getElementsByTag("td");
            Iterator<Element> tds = tdList.iterator();
            int applicantPlace = 0;
            totalApplicantCount = 0;
            totalTakeoutDocCount = 0;
            totalOtherRaceUsageCount = 0;
            originalDocsCount = 0;
            cheaterNumCount = 0;
            while (tds.hasNext()) {
                Applicant applicant = extractApplicant(tds);
                totalApplicantCount++;
                if (applicant != null) {
                    applicantPlace++;
                    applicantList.add(applicant);
                    if (lastName.equals(applicant.getLastName())) {
                        LOG.info("Place of {} is {}", lastName, applicantPlace);
                        return applicantPlace;
                    }
                }
            }
        } catch (IOException e) {
            LoggerFactory.getLogger(this.getClass()).error(e.getMessage(), e);
            throw new IllegalStateException(e);
        }
        return applicantList.size();
    }

    public int getTotalApplicantCount() {
        return totalApplicantCount;
    }

    public int getTotalTakeoutDocCount() {
        return totalTakeoutDocCount;
    }

    public int getTotalOtherRaceUsageCount() {
        return totalOtherRaceUsageCount;
    }

    public int getOriginalDocsCount() {
        return originalDocsCount;
    }

    public int getCheaterNumCount() {
        return cheaterNumCount;
    }

    protected Applicant extractApplicant(Iterator<Element> elementList) {
        Applicant applicant = new Applicant();

        int numberLine = Integer.parseInt(elementList.next().text().trim());  //1
        int idNum = Integer.parseInt(elementList.next().text().trim());       //2

        applicant.setLastName(elementList.next().text().trim());              //3
        applicant.setFirstName(elementList.next().text().trim());             //4
        applicant.setMiddleName(elementList.next().text().trim());            //5
        String scoreValue = elementList.next().text().trim();                 //6
        if (StringUtils.hasText(scoreValue)) {
            applicant.setScore(Integer.parseInt(scoreValue));
        }

        String condition = elementList.next().text().trim();                     //7
        if ("б/э".equals(condition) || "в/к".equals(condition)) {
            applicant.setCondition(ConditionEnum.PREFENTIAL);
            cheaterNumCount++;
        } else if ("общ.".equals(condition)) {
            applicant.setCondition(ConditionEnum.COMMON);
        } else {
            throw new IllegalArgumentException(condition);
        }

        String docPermit = elementList.next().text().trim();                     //8
        elementList.next();                                                  //10
        elementList.next();
        if ("Да".equals(docPermit)) {
            applicant.setOriginalDocs(Boolean.TRUE);
            originalDocsCount++;
        } else if ("Нет".equals(docPermit)) {
            applicant.setOriginalDocs(Boolean.FALSE);
        } else if ("Забрал Документы".equals(docPermit)) {
            totalTakeoutDocCount++;                                              //9
            return null;
        } else if ("Подал на другой конкурс".equals(docPermit)) {
            totalOtherRaceUsageCount++;
            return null;
        } else {
            throw new IllegalArgumentException(docPermit);
        }
        LOG.debug("{}.{}", numberLine, applicant);
        return applicant;
    }

    public static void main(String[] args) {
        List<Direction> directionList = new ArrayList<Direction>();

        directionList.add(new Direction("010400.2", "ПМПУ - Прикладная математика и информатика", 180,
                "http://cabinet.spbu.ru/Lists/1k_RatingLists/list1_1_1_12_434__0_0_0_0.html", EntranceSetEnum.MRI));

        directionList.add(new Direction("010800.0", "МатМех - Механика и математическое моделирование, МРИ", 22,
                "http://cabinet.spbu.ru/Lists/1k_RatingLists/list1_1_1_15_437__0_0_0_0.html", EntranceSetEnum.MRI));

        directionList.add(new Direction("010900.2", "ПМПУ - Прикладные математика, физика и процессы управления", 30,
                "http://cabinet.spbu.ru/Lists/1k_RatingLists/list1_1_1_1806_439__0_0_0_0.html", EntranceSetEnum.MRI));

        University university = new University("СПбГУ", "Университет");
        university.setDirectionList(directionList);

        SpbuPlaceCalculator calculator = new SpbuPlaceCalculator();
        for (Direction direction : directionList) {
            LOG.info("{}", direction);
            int place = calculator.calcPlace(direction.getUrl(), "Чуриков");
            LOG.info("{}:place={} from {}, out of race={}, take out docs = {}, original={}, cheaters={}",
                    new Object[] {direction.getId(), place,
                    direction.getTotalPlaces(), calculator.getTotalOtherRaceUsageCount(),
                calculator.getTotalTakeoutDocCount(), calculator.getOriginalDocsCount(),
                    calculator.getCheaterNumCount()});
        }

        System.exit(0);
    }
}
